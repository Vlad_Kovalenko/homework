/**
 * Created by Vlad on 12.11.2014.
 */
public class Boat extends Vehicle {

    @Override
    public void accelerate() {
        super.setSpeed(super.getSpeed() +5);
        super.setGas(super.getGas() -15);
        System.out.println("Gas = " + super.getGas());
        System.out.println("Speed = " + super.getSpeed());

    }

    @Override
    public void brake() {
        System.out.println("Speed 0");
    }

    @Override
    public void turn() {
        int turn=(int)Math.random()*3;
        if (turn == 1 ) System.out.println("rigt");
        else System.out.println("left");

    }
}
