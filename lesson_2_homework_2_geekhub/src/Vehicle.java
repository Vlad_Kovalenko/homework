/**
 * Created by Vlad on 12.11.2014.
 */
abstract public class Vehicle implements Driveable {
    private double gas =300;
    private double solar_battery =300;
    private int speed;

    public double getGas() {
        return gas;
    }

    public void setGas(double gas) {
        this.gas = gas;
    }

    public double getSolar_battery() {
        return solar_battery;
    }

    public void setSolar_battery(double solar_battery) {
        this.solar_battery = solar_battery;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }
}
