/**
 * Created by Vlad on 12.11.2014.
 */
public class SolarPoweredCar extends Vehicle {


    @Override
    public void accelerate() {
        super.setSolar_battery(super.getSolar_battery() -20);
        super.setSpeed(super.getSpeed() +10 );
        System.out.println("Solar_Battery = " + super.getSolar_battery());
        System.out.println("Speed = " + super.getSpeed());

    }

    @Override
    public void brake() {
        System.out.println("Speed 0");
    }

    @Override
    public void turn() {
        int turn=(int)Math.random()*3;
        if (turn == 1 ) System.out.println("rigt");
        else System.out.println("left");

    }
}
