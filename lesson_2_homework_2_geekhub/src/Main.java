import java.util.Scanner;

/**
 * Created by Vlad on 12.11.2014.
 */
public class Main {
    public static void main(String[] args) {
        System.out.println("1. Car");
        System.out.println("2 Boat");
        System.out.println("3 SolarPoweredCar");
        System.out.println("0 exit");
        Scanner scanner = new Scanner(System.in);

        switch (scanner.nextInt()){
            case 1:
                Car car = new Car();
                for(int i= 0;i< 20; i++){
                    car.accelerate();
                    car.turn();
                }
                car.brake();
                break;
            case 2:
                Boat boat = new Boat();
                for (int i= 0; i<10; i++){
                    boat.accelerate();
                    boat.turn();
                }
                boat.brake();

                break;
            case 3:
                SolarPoweredCar solarPoweredCar = new SolarPoweredCar();
                for(int i=0; i< 20; i++){
                    solarPoweredCar.accelerate();
                    solarPoweredCar.turn();
                }
                solarPoweredCar.brake();
                break;
        }






    }
}