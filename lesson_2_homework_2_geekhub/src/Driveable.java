/**
 * Created by Vlad on 12.11.2014.
 */
public interface Driveable {
    void accelerate();
    void brake();
    void turn();
}
