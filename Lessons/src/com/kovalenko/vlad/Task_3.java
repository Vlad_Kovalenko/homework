package com.kovalenko.vlad;

import java.util.Scanner;

/**
 * Created by Vlad on 19.10.2014.
 */
public class Task_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextByte();

        String[] digits = new String[]{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};
        System.out.println(digits[n]);

        switch (n) {
            case 0:
                System.out.println("zero");
                break;
            case 1:
                System.out.println("one");
                break;
            case 2:
                System.out.println("two");
                break;
            case 3:
                System.out.println("three");
                break;
            case 4:
                System.out.println("four");
                break;
            case 5:
                System.out.println("five");
                break;
            case 6:
                System.out.println("six");
                break;
            case 7:
                System.out.println("seven");
                break;
            case 8:
                System.out.println("eight");
                break;
            case 9:
                System.out.println("nine");
                break;
            case 10:
                System.out.println("ten");
                break;
        }

        if (n == 0) {
            System.out.println("zero");
        } else if (n == 1) {
            System.out.println("one");
        } else if (n == 2) {
            System.out.println("two");
        } else if (n == 3) {
            System.out.println("three");
        } else if (n == 4) {
            System.out.println("four");
        } else if (n == 5) {
            System.out.println("five");
        } else if (n == 6) {
            System.out.println("six");
        } else if (n == 7) {
            System.out.println("seven");
        } else if (n == 8) {
            System.out.println("eight");
        } else if (n == 9) {
            System.out.println("nine");
        } else if (n == 10) {
            System.out.println("ten");
        }

    }


}
