package com.kovalenko.vlad;

import java.util.Scanner;

/**
 * Created by Vlad on 19.10.2014.
 */
public class Task_1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println(factorial(scanner.nextInt()));
    }

    public static int factorial(int x) {
        if (x < 0) return -1;
        int fact = 1;
        for (int i = 2; i <= x; i++)
            fact = fact * i;
        return fact;
    }
}
